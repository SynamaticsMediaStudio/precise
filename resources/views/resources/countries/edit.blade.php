@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Edit {{$country->name}}</h3>
        <a href="{{route('country.show',[$country->id])}}" class="float-right btn btn-danger">Cancel</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('country.update',[$country->id])}}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="precise_country_name">Name</label>
                                <input type="text" name="name" id="precise_country_name" class="form-control" value="@if(old('name')) old('name') @else {{$country->name}}  @endif">
                            </div>
                            <div class="form-group">
                                <label for="precise_status">Status</label>
                                <select name="status" id="precise_status" class="form-control">
                                    <option @if(old('status') == '1' || $country->status == '1') selected @endif value="1">Active</option>
                                    <option @if(old('status') == '0' || $country->status == '0') selected @endif value="0">InActive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection