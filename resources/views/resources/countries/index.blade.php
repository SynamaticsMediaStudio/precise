@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Countries</h3>
        <a href="{{route('country.create')}}" class="float-right btn btn-info">Create</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <table class="table table-sm">
                        <thead class="table-dark">
                            <tr>
                                <th>Country</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                           @forelse ($countries as $country)
                               <tr>
                                   <th><a href="{{route('country.show',[$country->id])}}">{{$country->name}}</a></th>
                                   <th>{!!$country->Status()!!}</th>
                               </tr>
                           @empty
                               <th class="text-center" colspan="2">
                                   No countries Listed
                               </th>
                           @endforelse
                        </tbody>
                    </table>
                    <div class="card-footer">
                        {{$countries->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection