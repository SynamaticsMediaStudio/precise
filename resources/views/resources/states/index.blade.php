@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>States</h3>
        <a href="{{route('state.create')}}" class="float-right btn btn-info">Create</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <table class="table table-sm">
                        <thead class="table-dark">
                            <tr>
                                <th>State</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                           @forelse ($states as $state)
                               <tr>
                                   <th><a href="{{route('state.show',[$state->id])}}">{{$state->name}}</a></th>
                                   <th>{!!$state->Status()!!}</th>
                               </tr>
                           @empty
                               <th class="text-center" colspan="2">
                                   No states Listed
                               </th>
                           @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection