@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>{{$city->name}}</h3>
        <small>{{$city->State->name}}, {{$city->State->Country->name}}</small><br>
        {!!$city->Status()!!}
        <div class="float-right btn-group">
            <a href="{{route('city.index')}}" class="btn btn-sm btn-danger">Back</a>
            <a href="{{route('city.edit',[$city->id])}}" class="btn btn-sm btn-outline-info">Edit</a>
            <form action="{{route('city.destroy',[$city->id])}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
            </form>
        </div>
    </div>
    </div>
@endsection