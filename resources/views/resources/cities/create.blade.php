@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Create new city</h3>
        <a href="{{route('city.index')}}" class="float-right btn btn-danger">Back</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('city.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="precise_city_name">Name</label>
                                <input type="text" name="name" id="precise_city_name" class="form-control" value="{{old('name')}}">
                            </div>
                            <div class="form-group">
                                <label for="precise_status">Status</label>
                                <select name="status" id="precise_status" class="form-control">
                                    <option @if(old('status') == '1') selected @endif value="1">Active</option>
                                    <option @if(old('status') == '0') selected @endif value="0">InActive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="precise_state">Status</label>
                                <select name="state" id="precise_state" class="form-control" required>
                                        <option selected disabled value="0">-- Select state --</option>
                                        @forelse ($countries as $country)
                                        <optgroup label="{{$country->name}}">
                                            @forelse ($country->States as $state)
                                                <option @if(old('state') == '{{$state->id}}') selected @endif value="{{$state->id}}">{{$state->name}}</option>
                                            @empty
                                            @endforelse
                                        </optgroup>
                                        @empty
                                        <option selected disabled value="0">--No states Available --</option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection