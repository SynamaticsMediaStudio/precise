<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::prefix('resource')->group(function () {
    Route::resource('country', 'CountryController');
    Route::resource('state', 'StateController');
    Route::resource('city', 'CityController');
    Route::resource('campus', 'CampusController');
    Route::resource('building', 'BuildingController');
    Route::resource('zone', 'ZoneController');
    Route::resource('area', 'AreaController');
    Route::resource('room', 'RoomController');
    Route::resource('seat', 'SeatController');
});
Auth::routes();