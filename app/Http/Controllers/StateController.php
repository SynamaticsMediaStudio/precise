<?php

namespace App\Http\Controllers;

use App\State;
use App\Country;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::paginate(10);
        return view("resources.states.index",['states'=>$states]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view("resources.states.create",['countries'=>$countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "country"=>"int|required",
            "status"=>"int",
        ]);
        $state = new State;
        $state->name = $request->name;
        $state->status = $request->status;
        $state->country = $request->country;
        $state->save();
        return redirect()->route('state.show',[$state->id])->with(['status'=>"New State for $state->Country Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        return view("resources.states.show",["state"=>$state]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        $countries = Country::all();
        return view("resources.states.edit",["state"=>$state,"countries"=>$countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        $request->validate([
            "name"=>"required",
            "country"=>"int|required",
            "status"=>"int",
        ]);
        $state->name = $request->name;
        $state->status = $request->status;
        $state->country = $request->country;
        $state->save();
        return redirect()->route('state.show',[$state->id])->with(['status'=>"Updated $state->name"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        $state->delete();
        return redirect()->route('state.index')->with(['status'=>"State Deleted"]);
    }
}
